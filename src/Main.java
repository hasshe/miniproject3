import java.util.ArrayList;
import java.util.List;

import command.AddHeading;
import command.AddList;
import command.AddListElt;
import command.AddParagraph;
import command.Broker;
import command.CreateDocument;
import command.CreateHTML;
import command.CreateList;
import command.CreateMD;
import command.CreateMail;
import command.CreatePM;
import command.Print;
import facade.Facade;
import factory.Factory;
import items.HeadingItem;
import items.ListItem;
import items.ParagraphItem;

public class Main {
   public static void main(String[] args) {

	   Factory f = new Factory();
	   
	   /**
	   Facade r = new Facade();
	   ParagraphItem pi = f.createParagraph();
	   pi.setData("Value");
	   r.createList();
	   r.addListElt(pi);
	   r.createDocument();
	   **/
	   ListItem l = f.createList();
	   Broker broker = new Broker();
	   Factory factory = new Factory();
	   List<String> list = new ArrayList<String>();
	   list.add("Jello");
	   list.add("Hello");
	   list.add("tllo");
	   list.add("yyyy");
	   list.add("hhhhh");
	   broker.executeOperation(new CreateDocument());
	   broker.executeOperation(new CreateList());
	   broker.executeOperation(new CreatePM("1", list, "2", "3", "4", "5", "6"));
		/*
		 * ParagraphItem paragraph = factory.createParagraph();
		 * paragraph.setData("Im a list element"); broker.executeOperation(new
		 * AddListElt(paragraph));
		 */
	   broker.undoOperation();
//	   broker.executeOperation(new AddList());
//	   broker.executeOperation(new AddHeading("Hello"));
//	   broker.executeOperation(new AddParagraph("there")); 
//	   broker.executeOperation(new AddParagraph("there")); 
//	   broker.executeOperation(new AddParagraph("there")); 
	 //  broker.undoOperation();
	   broker.redoOperation();
//	   broker.executeOperation(new Print());
	   broker.executeOperation(new CreateMD());
	   	   
	   /**
	  // Visitable computer = new Document();
	   Factory factory = new Factory();
	   Facade facade = Facade.getInstance();
	   ParagraphItem paragraphItem = factory.createParagraph();
	   paragraphItem.setData("Value");
	   facade.createList();
	   facade.addListElt(paragraphItem);
	   facade.createDocument();
	   
	   ListItem listItem = factory.createList();

	   //ParagraphItem pi = f.createParagraph();
	   ParagraphItem pi2 = factory.createParagraph();
	   ParagraphItem pi3 = factory.createParagraph();
	   HeadingItem h1 = factory.createHeading();
	  // pi.setData("Hello");
	   facade.addHeading("HelloWorld");
	   pi2.setData("Hello2");
	   pi3.setData("Hello3");
	   facade.addParagraph("Hello");
	   facade.addList();
	  // l.add(pi);
	   listItem.add(pi2);
	   listItem.add(pi3);
	   facade.addListElt(pi2);
	   facade.addListElt(pi3);
	  // r.print();
	   //r.createHTML();
	   //r.createMD();
	   List<String> list = new ArrayList<String>();
	   list.add("Jello");
	   list.add("Hello");
	   list.add("tllo");
	   list.add("yyyy");
	   list.add("hhhhh");
	  
	   //r.createMailHTML("Mail Heading", "My text");
	   
	   //facade.createPMMD("Title", list,"Summary", "Introduc", "Method", "Result", "Discussion");
	   facade.createMail("Header", "Bread");
	   facade.createMD();
	   **/
   }
}