package command;

import facade.Facade;

public class Print implements Command{
	
	Facade facade;
	
	public Print() {
		facade = Facade.getInstance();
	}

	@Override
	public void redo() {
		facade.print();
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub
		
	}

}
