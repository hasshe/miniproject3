package command;

import facade.Facade;

public class AddList implements Command {
	
	private Facade facade;
	
	public AddList() {
		facade = Facade.getInstance();
	}

	@Override
	public void redo() {
		facade.addList();;	
	}

	@Override
	public void undo() {
		facade.removeLatestContent();
	}

}
