package command;

import facade.Facade;

public class CreateDocument implements Command {

	Facade facade;
	
	public CreateDocument() {
		facade = Facade.getInstance();
	}
	
	@Override
	public void redo() {
		facade.createDocument();
	}

	@Override
	public void undo() {
		
		
	}

}
