package command;

import facade.Facade;

public class CreateMD implements Command {

	Facade facade;
	
	public CreateMD() {
		facade = Facade.getInstance();
	}
	
	@Override
	public void redo() {
		facade.createMD();
	}

	@Override
	public void undo() {
	}
}
