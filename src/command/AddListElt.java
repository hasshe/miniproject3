package command;

import facade.Facade;
import items.Items;

public class AddListElt implements Command {
	
	private Facade facade;
	private Items<?> items;
	
	public AddListElt(Items<?> item) {
		facade = Facade.getInstance();
		this.items = item;
	}

	@Override
	public void redo() {
		facade.addListElt(items);;		
	}

	@Override
	public void undo() {
		facade.removeListElt();
	}

}
