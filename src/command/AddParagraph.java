package command;

import facade.Facade;

public class AddParagraph implements Command {
	
	private String text;
	private Facade facade;
	
	public AddParagraph(String text) {
		facade = Facade.getInstance();
		this.text = text;
	}

	@Override
	public void redo() {
		facade.addParagraph(text);		
	}

	@Override
	public void undo() {
		facade.removeLatestContent();
	}

}
