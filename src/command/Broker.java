package command;

import java.util.ArrayList;
import java.util.List;

public class Broker {
	
	private List<Command> undo;
	private List<Command> redo;
	
	public Broker() {
		undo = new ArrayList<Command>();
		redo = new ArrayList<Command>();
	}
	
	public void executeOperation(Command command) {
		command.redo();
		undo.add(command);
		redo.clear();
	}
	
	public void undoOperation() {
		if(!undo.isEmpty()) {
			Command operation = undo.get(undo.size()-1);
			operation.undo();
			redo.add(operation);
			undo.remove(undo.size()-1);
		}
	}
	
	public void redoOperation() {
		if(!redo.isEmpty()) {
			Command operation = redo.get(redo.size()-1);
			operation.redo();
			undo.add(operation);
			redo.remove(redo.size()-1);
		}
	}
}
