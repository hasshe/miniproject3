package command;

import facade.Facade;

public class CreateList implements Command {

	Facade facade;
	
	public CreateList() {
		facade = Facade.getInstance();
	}
	
	@Override
	public void redo() {
		facade.createList();
	}

	@Override
	public void undo() {
		facade.removeList();
		
	}

}
