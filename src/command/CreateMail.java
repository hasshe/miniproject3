package command;
import facade.Facade;

public class CreateMail implements Command {
	
	private Facade facade;
	private String header;
	private String text;
	
	
	public CreateMail(String header, String text) {	
		facade = Facade.getInstance();
		this.header = header;
		this.text = text;

	}

	@Override
	public void redo() {
		facade.createMail(header, text);		
	}

	@Override
	public void undo() {
		for(int i = 0; i < 2; i++) {
			facade.removeLatestContent();
		}
	}

}
