package command;

public interface Command {

	public void redo();
	public void undo();
	
}
