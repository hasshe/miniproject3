package command;

import facade.Facade;

public class AddHeading implements Command {
	
	private String text;
	private Facade facade;
	
	public AddHeading(String text) {
		facade = Facade.getInstance();
		this.text = text;
	}

	@Override
	public void redo() {
		facade.addHeading(text);	
	}

	@Override
	public void undo() {
		facade.removeLatestContent();
	}

}
