package command;

import facade.Facade;

public class CreateHTML implements Command {

	Facade facade;
	
	public CreateHTML() {
		facade = Facade.getInstance();
	}
	
	@Override
	public void redo() {
		facade.createHTML();
	}

	@Override
	public void undo() {
		
		
	}

}
