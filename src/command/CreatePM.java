package command;

import java.util.List;

import facade.Facade;

public class CreatePM implements Command {
	
	private Facade facade;
	private String title;
	private List<String> list;
	private String summary;
	private String intro;
	private String method;
	private String result;
	private String discussion;
	
	
	public CreatePM(String title, List<String> list, String summary, String intro, String method, String result, 
			String discussion) {	
		facade = Facade.getInstance();
		this.title = title;
		this.list = list;
		this.summary = summary;
		this.intro = intro;
		this.method = method;
		this.result = result;
		this.discussion = discussion;
	}

	@Override
	public void redo() {
		facade.createPM(title, list, summary, intro, method, result, discussion);		
	}

	@Override
	public void undo() {
		for(int i = 0; i < 7; i++) {
			facade.removeLatestContent();
		}
	}

}
