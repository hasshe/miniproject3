package facade;
import java.util.List;

import builder.MailBuilder;
import builder.PMBuilder;
import document.Document;
import factory.Factory;
import items.HeadingItem;
import items.Items;
import items.ListItem;
import items.ParagraphItem;
import visitor.VisitorDocument;

public class Facade {

	private static Facade instance = new Facade();
	private Factory factory = new Factory();
	private ListItem listItem;
	private Document document;
	private ParagraphItem paraItem;
	private HeadingItem headItem;
	
	private Facade() {}
	
	public static Facade getInstance() {
		return instance;
	}

	public void addParagraph(String text) {
		paraItem = factory.createParagraph();
		paraItem.setData(text);
		document.addParagraph(paraItem);
	}

	public void addHeading(String text) {
		headItem = factory.createHeading();
		headItem.setData(text);
		document.addHeading(headItem);
	}

	public void addListElt(Items<?> text) {
		listItem.add(text);
	}
	
	public void removeListElt() {
		listItem.remove();
	}

	public void addList() {
		document.addList(listItem);
	}

	public void createList() {
		listItem = factory.createList();
	}
	
	public void removeList() {
		listItem = null;
	}

	public void createDocument() {
		document = factory.createDocument();
	}
	
	public void removeLatestContent() {
		document.removeLatestContent();
	}
	

	public void print() {
		for(Items<?> item: document.getDocument()) {
			System.out.println(item.getData());
		}
	}

	public void createHTML() {
		VisitorDocument visit = new VisitorDocument(document.getDocument());
		visit.accept(factory.createHTML());
	}

	public void createMD() {
		VisitorDocument visit = new VisitorDocument(document.getDocument());
		visit.accept(factory.createMD());
	}
	
	public void createPM(String title, List<String> list, String summary, String intro, String method, String result, 
			String discussion) {
		document.add(new PMBuilder.Builder().setTitle(getHeading(title)).setList(getListItem(list)).setSummary(getHeading(summary))
				.setIntro(getHeading(intro)).setMethod(getHeading(method)).setResult(getHeading(result)).setDiscussion(getHeading(discussion)).build().getData());
	}
	
	public void createMail(String header, String text) {
		document.add(new MailBuilder.Builder().setHeader(getHeading(header)).setText(getParagraph(text)).build().getData());
	}
	
	private HeadingItem getHeading(String txt) {
		headItem = factory.createHeading();
		headItem.setData(txt);
		return headItem;
	}
	
	private ParagraphItem getParagraph(String txt) {
		paraItem = factory.createParagraph();
		paraItem.setData(txt);
		return paraItem;
	}
	
	private ListItem getListItem(List<String> list) {
		createList();
		for(String item : list) {
			addListElt(getParagraph(item));
		}
		return listItem;
	}
}
