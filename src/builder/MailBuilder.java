package builder;
import java.util.ArrayList;
import java.util.List;

import items.HeadingItem;
import items.Items;
import items.ParagraphItem;

@SuppressWarnings("rawtypes")
public class MailBuilder {

	private HeadingItem header;
	private ParagraphItem text;
	private List<Items> builtList = new ArrayList<Items>();

	public HeadingItem getHeader() {
		return this.header;
	}

	public ParagraphItem getText() {
		return this.text;
	}

	public List<Items> getData() {
		builtList.add(getHeader());
		builtList.add(getText());
		return builtList;
	}

	public MailBuilder(Builder builder) {
		this.header = builder.header;
		this.text = builder.text;
	}

	public static class Builder {

		private HeadingItem header;
		private ParagraphItem text;

		public Builder() {}
		
		public Builder setHeader(HeadingItem header) {
			this.header = header;
			return this;
		}
		
		public Builder setText(ParagraphItem text) {
			this.text = text;
			return this;
		}

		public MailBuilder build() {
			return new MailBuilder(this);
		}

	}

}
