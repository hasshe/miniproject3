package builder;
import java.util.ArrayList;
import java.util.List;

import items.HeadingItem;
import items.Items;
import items.ListItem;
import items.ParagraphItem;

@SuppressWarnings("rawtypes")
public class PMBuilder {

	private ParagraphItem text;
	private HeadingItem title, intro, method, result, discussion, summary;
	private ListItem list;
	private List<Items> builtList = new ArrayList<Items>();

	public ParagraphItem getText() {
		return text;
	}

	public HeadingItem getTitle() {
		return title;
	}

	public HeadingItem getIntro() {
		return intro;
	}

	public HeadingItem getMethod() {
		return method;
	}

	public HeadingItem getResult() {
		return result;
	}

	public HeadingItem getDiscussion() {
		return discussion;
	}

	public HeadingItem getSummary() {
		return summary;
	}

	public ListItem getList() {
		return list;
	}

	public List<Items> getData() {
		builtList.add(title);
		builtList.add(list);
		builtList.add(summary);
		builtList.add(intro);
		builtList.add(method);
		builtList.add(result);
		builtList.add(discussion);
		return builtList;
	}

	private PMBuilder(Builder builder) {
		this.intro = builder.intro;
		this.discussion = builder.discussion;
		this.title = builder.title;
		this.method = builder.method;
		this.list = builder.list;
		this.result = builder.result;
		this.summary = builder.summary;
	}

	public static class Builder {

		private HeadingItem title, intro, method, result, discussion, summary;
		private ListItem list;

		public Builder(){}
		
		public Builder setTitle(HeadingItem title) {
			this.title = title;
			return this;
		}
		public Builder setList(ListItem list) {
			this.list = list;
			return this;
		}
		public Builder setSummary(HeadingItem summary) {
			this.summary = summary;
			return this;
		}
		public Builder setIntro(HeadingItem intro) {
			this.intro = intro;
			return this;
		}
		public Builder setMethod(HeadingItem method) {
			this.method = method;
			return this;
		}
		public Builder setResult(HeadingItem result) {
			this.result = result;
			return this;
		}
		public Builder setDiscussion(HeadingItem discussion) {
			this.discussion = discussion;
			return this;
		}

		public PMBuilder build() {
			return new PMBuilder(this);
		}

	}

}