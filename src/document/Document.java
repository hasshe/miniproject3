package document;
import java.util.ArrayList;
import java.util.List;

import items.Items;

@SuppressWarnings("rawtypes")
public class Document {
	
	private List<Items> list = new ArrayList<Items>();
	
	public void addParagraph(Items<?> li) {
		list.add(li);
	}
	public void addHeading(Items<?> li) {
		list.add(li);
	}
	public void addList(Items<?> li) {
		list.add(li);
	}
	public List<Items> getDocument() {
		return list;
	}
	
	public void add(List<Items> i) {
		list.addAll(i);
	}
	
	public void removeLatestContent() {
		if(!list.isEmpty())
		list.remove(list.size()-1);
	}
}
