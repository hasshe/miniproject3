package factory;
import document.Document;
import items.HeadingItem;
import items.ListItem;
import items.ParagraphItem;
import visitor.HTMLVisitor;
import visitor.MDVisitor;

public class Factory {
	
	public HeadingItem createHeading() {
		return new HeadingItem();
	}
	
	public ParagraphItem createParagraph() {
		return new ParagraphItem();
	}
	
	public ListItem createList() {
		return new ListItem();
	}
	
	public HTMLVisitor createHTML() {
		return new HTMLVisitor();
	}
	
	public MDVisitor createMD() {
		return new MDVisitor();
	}
	
	public Document createDocument() {
		return new Document();
	}

}
