package items;
import visitor.Visitor;

public class ParagraphItem implements Items<Object> {
	
	private String txt;

	@Override
	public void accept(Visitor visitor) {
		visitor.visitParagraph(this);
	}
	
	@Override
	public void setData(String text) {
		this.txt = text;

	}
	@Override
	public String getData() {
		return txt;
	}
}