package items;
import java.util.ArrayList;
import java.util.List;

import visitor.Visitor;


@SuppressWarnings("rawtypes")
public class ListItem implements Items<Object> {
	
	
	private List<Items> list;
	private Items<?> item;

	public ListItem() {
		list = new ArrayList<Items>();
	}

	public void add(Items<?> text) {
		this.item = text;
		list.add(text);
	}
	
	public void remove() {
		this.item = null;
		list.remove(list.size()-1);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visitList(this);
	}

	@Override
	public void setData(String text) {
		item.setData(text);

	}

	@Override
	public List<Items> getData() {
		return list;
	}
}