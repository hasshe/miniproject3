package items;
import visitor.Visitable;

public interface Items<T> extends Visitable{

	void setData(String text);
	
	T getData();
}
