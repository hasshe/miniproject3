package visitor;
import items.HeadingItem;
import items.Items;
import items.ListItem;
import items.ParagraphItem;

public class MDVisitor implements Visitor {

	@Override
	public void visitHeading(HeadingItem hi) {
		System.out.print("<mh1>" + hi.getData() + "</mh1>\n");
	}

	@Override
	public void visitParagraph(ParagraphItem pi) {
		System.out.print("<mdp>" + pi.getData() + "</mdp>\n");
	}

	@Override
	public void visitList(ListItem li) {
		for(Items<?> s: li.getData()) {
			   System.out.print("<mdli>" + s.getData() + "<mdli>\n");
		 }
	}

	@Override
	public void visitDocument(VisitorDocument vDoc) {
	}

}
