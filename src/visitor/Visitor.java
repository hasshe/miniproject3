package visitor;
import items.HeadingItem;
import items.ListItem;
import items.ParagraphItem;

public interface Visitor {
	public void visitHeading(HeadingItem hi);
	public void visitParagraph(ParagraphItem pi);
	public void visitList(ListItem li);
	public void visitDocument(VisitorDocument vDoc);
}