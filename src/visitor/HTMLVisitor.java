package visitor;
import items.HeadingItem;
import items.Items;
import items.ListItem;
import items.ParagraphItem;

public class HTMLVisitor implements Visitor {

	@Override
	public void visitHeading(HeadingItem hi) {
		System.out.print("<h1>" + hi.getData() + "</h1>\n");
	}

	@Override
	public void visitParagraph(ParagraphItem pi) {
		System.out.print("<p>" + pi.getData() + "</p>\n");
	}

	@Override
	public void visitList(ListItem li) {
		for(Items<?> s: li.getData()) {
			System.out.print("<li>" + s.getData() + "<li>\n");
		}
	}

	@Override
	public void visitDocument(VisitorDocument vDoc) {
	}
}