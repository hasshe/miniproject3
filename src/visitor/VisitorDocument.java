package visitor;
import java.util.List;

import items.Items;

@SuppressWarnings("rawtypes")
public class VisitorDocument implements Visitable {

	private List<Items> list;
	private Visitor visit;

	public VisitorDocument(List<Items> document) {
		this.list = document;
	}

	@Override
	public void accept(Visitor visit) {
		for (int i = 0; i < list.size(); i++) {
			list.get(i).accept(visit);
		}
		visit.visitDocument(this);
	}
}